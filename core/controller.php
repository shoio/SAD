<?php
class controller{
	private $professor;

	public function __construct(){
		$p = new Professores();
		$this->$professor = $p->getDados();
	}

	public function getProfessor(){
		$id = $_SESSION['lg'];
		
		$sql = "SELECT * FROM professores WHERE id = '$id'";
		$sql = $this->db->query($sql);

		if ($sql->rowCount() > 0) {
			
			return $sql->fetch();
		}
	}

	public function loadView($viewName, $viewData = array()){

		//EXTRACT transforma as keys do array em variáveis com seus respectivos valores.
		extract($viewData); 
		include 'views/'.$viewName.'.php';
	}

	public function loadTemplate($viewName, $viewData = array()){
		include 'views/template.php';
	}

	public function loadViewInTemplate($viewName, $viewData = array()){
		extract($viewData);
		include 'views/'.$viewName.'.php';
	}
}