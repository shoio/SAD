
<h3>Bem vindo(a) professor(a) <a href="<?php echo BASE.'home';?>"><?php echo $professor['nome'] ?></a> <a class="btn btn-danger pull-right" href="<?php echo BASE.'home/logout' ?>"><span class="glyphicon glyphicon-off"></span> SAIR</a></h3><hr>

<div class="col-sm-3 col-md-3">
	<ul class="list-group">
	  <li class="list-group-item list-group-item-info text-center"><strong>TURMAS</strong></li>
	  <li class="list-group-item"><a href="<?php echo BASE.'home/turma/6' ?>">6° ano</a></li>
	  <li class="list-group-item"><a href="<?php echo BASE.'home/turma/7' ?>">7° ano</a></li>
	  <li class="list-group-item"><a href="<?php echo BASE.'home/turma/8' ?>">8° ano</a></li>
	  <li class="list-group-item"><a href="<?php echo BASE.'home/turma/9' ?>">9° ano</a></li>
	  <li class="list-group-item list-group-item-info text-center"><a href="<?php echo BASE.'home/professores/' ?>"><strong>PROFESSORES</strong></a></li>
	 </ul>
</div>

<div class="col-sm-9 col-md-9">
	<form method="POST" role="form">
			<legend>Editar Aluno</legend>
		
			<div class="form-group">
				<label for="nome">Nome: </label>
				<input type="text" class="form-control" name="nome" value="<?php echo $aluno['nome']; ?>">
			</div>
			<br>
			<div class="form-group">
				<label for="">Turma: </label>
				<select name="turma">
					<option value="6" <?php if($aluno['turma'] == 6){echo 'selected';} ?>>6° ano</option>
					<option value="7" <?php if($aluno['turma'] == 7){echo 'selected';} ?>>7° ano</option>
					<option value="8" <?php if($aluno['turma'] == 8){echo 'selected';} ?>>8° ano</option>
					<option value="9" <?php if($aluno['turma'] == 9){echo 'selected';} ?>>9° ano</option>
				</select>
			</div>
					
			<br><br><br>
			<button type="submit" class="btn btn-info">Salvar</button>
		</form>	
</div>