<h3>Bem vindo(a) professor(a) <a href="<?php echo BASE.'home';?>"><?php echo $professor['nome'] ?></a> <a class="btn btn-danger pull-right" href="<?php echo BASE.'home/logout' ?>"><span class="glyphicon glyphicon-off"></span> SAIR</a></h3><hr>

<div class="col-sm-3 col-md-3">
	<ul class="list-group">
	  <li class="list-group-item list-group-item-info text-center"><strong>TURMAS</strong></li>
	  <li class="list-group-item"><a href="<?php echo BASE.'home/turma/6' ?>">6° ano</a></li>
	  <li class="list-group-item"><a href="<?php echo BASE.'home/turma/7' ?>">7° ano</a></li>
	  <li class="list-group-item"><a href="<?php echo BASE.'home/turma/8' ?>">8° ano</a></li>
	  <li class="list-group-item"><a href="<?php echo BASE.'home/turma/9' ?>">9° ano</a></li>
	  <li class="list-group-item list-group-item-info text-center"><a href="<?php echo BASE.'home/professores/' ?>"><strong>PROFESSORES</strong></a></li>
	 </ul>
</div>
	
<div class="col-sm-9 col-md-9">
	<form method="POST" role="form">
		<legend>Avaliação Descritiva</legend>
		
		<div class="form-group">
			<label for="">Nome do Professor:</label>
			<input type="text" class="form-control" name="nome_p" value="<?php echo $professor['nome']; ?>" readonly="readonly">
		</div>

		<div class="form-group">
			<input type="hidden" class="form-control" name="id_professor" value="<?php echo $professor['id']; ?>" >
		</div>

		<div class="form-group">
			<label for="">Matéria:</label>
			<input type="text" class="form-control" name="materia" value="<?php echo $professor['materia']; ?>" readonly="readonly"> 
		</div>

		<div class="form-group">
			<label for="">Bimestre:</label>
			<input type="text" class="form-control" name="bimestre" value="<?php echo $bimestre; ?>" readonly="readonly"> 
		</div>

		<div class="form-group">
			<label for="">ID Aluno</label>
			<input type="text" class="form-control" name="id_aluno" value="<?php echo $aluno['id']; ?>" readonly="readonly"> 
		</div>

		<div class="form-group">
			<label for="">Nome do Aluno:</label>
			<input type="text" class="form-control" name="nome_a" value="<?php echo $aluno['nome']; ?>" readonly="readonly">
		</div>

		<div class="form-group">
			<label for="">Série:</label>
			<input type="text" class="form-control" name="serie" value="<?php echo $aluno['turma']; ?>" readonly="readonly"> 
		</div>

		<br><hr><br>
		
		
		<div class="form-group">
			<?php $perg = $perguntas[0]; ?>
			<label for=""><?php echo $perg['pergunta']; ?></label>
			<input type="hidden" name="perg_um" value="<?php echo $perg['pergunta']; ?>">
			
			<div class="radio">
 			 <label><input type="radio" name="resp_um" value="Não">Não.</label>
 			
			</div>
			<div class="radio">
 			 <label><input type="radio" name="resp_um" value="Frequentemente não">Frequentemente não.</label>
 			
			</div>
			<div class="radio">
 			 <label><input type="radio" name="resp_um" value="Quase sempre">Quase sempre.</label>
 			
			</div>
			<div class="radio">
 			 <label><input type="radio" name="resp_um" value="Sempre">Sempre.</label>
 			
			</div><hr>
		</div>


		<div class="form-group">
			<?php $perg = $perguntas[1]; ?>
			<label for=""><?php echo $perg['pergunta']; ?></label>
			<input type="hidden" name="perg_dois" value="<?php echo $perg['pergunta']; ?>">
			
			<div class="radio">
 			 <label><input type="radio" name="resp_dois" value="Não">Não.</label>
 			
			</div>
			<div class="radio">
 			 <label><input type="radio" name="resp_dois" value="Frequentemente não">Frequentemente não.</label>
 			
			</div>
			<div class="radio">
 			 <label><input type="radio" name="resp_dois" value="Quase sempre">Quase sempre.</label>
 			
			</div>
			<div class="radio">
 			 <label><input type="radio" name="resp_dois" value="Sempre">Sempre.</label>
 			
			</div><hr>
		</div>



		<div class="form-group">
			<?php $perg = $perguntas[2]; ?>
			<label for=""><?php echo $perg['pergunta']; ?></label>
			<input type="hidden" name="perg_tres" value="<?php echo $perg['pergunta']; ?>">
			
			<div class="radio">
 			 <label><input type="radio" name="resp_tres" value="Não">Não.</label>
 			
			</div>
			<div class="radio">
 			 <label><input type="radio" name="resp_tres" value="Frequentemente não">Frequentemente não.</label>
 			
			</div>
			<div class="radio">
 			 <label><input type="radio" name="resp_tres" value="Quase sempre">Quase sempre.</label>
 			
			</div>
			<div class="radio">
 			 <label><input type="radio" name="resp_tres" value="Sempre">Sempre.</label>
 			
			</div><hr>
		</div>



		<div class="form-group">
			<?php $perg = $perguntas[3]; ?>
			<label for=""><?php echo $perg['pergunta']; ?></label>
			<input type="hidden" name="perg_quatro" value="<?php echo $perg['pergunta']; ?>">
			
			<div class="radio">
 			 <label><input type="radio" name="resp_quatro" value="Não">Não.</label>
 			
			</div>
			<div class="radio">
 			 <label><input type="radio" name="resp_quatro" value="Frequentemente não">Frequentemente não.</label>
 			
			</div>
			<div class="radio">
 			 <label><input type="radio" name="resp_quatro" value="Quase sempre">Quase sempre.</label>
 			
			</div>
			<div class="radio">
 			 <label><input type="radio" name="resp_quatro" value="Sempre">Sempre.</label>
 			
			</div><hr>
		</div>



		<div class="form-group">
			<?php $perg = $perguntas[4]; ?>
			<label for=""><?php echo $perg['pergunta']; ?></label>
			<input type="hidden" name="perg_cinco" value="<?php echo $perg['pergunta']; ?>">
			
			<div class="radio">
 			 <label><input type="radio" name="resp_cinco" value="Não">Não.</label>
 			
			</div>
			<div class="radio">
 			 <label><input type="radio" name="resp_cinco" value="Frequentemente não">Frequentemente não.</label>
 			
			</div>
			<div class="radio">
 			 <label><input type="radio" name="resp_cinco" value="Quase sempre">Quase sempre.</label>
 			
			</div>
			<div class="radio">
 			 <label><input type="radio" name="resp_cinco" value="Sempre">Sempre.</label>
 			
			</div><hr>
		</div>



		<div class="form-group">
			<?php $perg = $perguntas[5]; ?>
			<label for=""><?php echo $perg['pergunta']; ?></label>
			<input type="hidden" name="perg_seis" value="<?php echo $perg['pergunta']; ?>">
			
			<div class="radio">
 			 <label><input type="radio" name="resp_seis" value="Não">Não.</label>
 			
			</div>
			<div class="radio">
 			 <label><input type="radio" name="resp_seis" value="Frequentemente não">Frequentemente não.</label>
 			
			</div>
			<div class="radio">
 			 <label><input type="radio" name="resp_seis" value="Quase sempre">Quase sempre.</label>
 			
			</div>
			<div class="radio">
 			 <label><input type="radio" name="resp_seis" value="Sempre">Sempre.</label>
 			
			</div><hr>
		</div>



		<div class="form-group">
			<?php $perg = $perguntas[6]; ?>
			<label><?php echo $perg['pergunta']; ?></label>
			<input type="hidden" name="perg_sete" value="<?php echo $perg['pergunta']; ?>">
  			<textarea class="form-control" rows="6" name="resp_sete" placeholder="Digite aqui uma observação a respeito do aluno, se houver alguma."></textarea>
		</div>
		
		<?php 
			$turma = $aluno['turma'];
			$nome = $aluno['nome'];
			$ano = date('Y');
		?>
	
		
		<br>
		<a href="<?php echo BASE."home/avaliarturma/$turma/$bimestre/$ano/$nome"; ?>"><button type="submit" class="btn btn-primary">Salvar</button></a>
		<br><br>
	</form>
</div>
