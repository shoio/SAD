
<h3>Bem vindo(a) professor(a) <a href="<?php echo BASE.'home';?>"><?php echo $professor['nome'] ?></a> <a class="btn btn-danger pull-right" href="<?php echo BASE.'home/logout' ?>"><span class="glyphicon glyphicon-off"></span> SAIR</a></h3><hr>

<div class="col-sm-3 col-md-3">
	<ul class="list-group">
	  <li class="list-group-item list-group-item-info text-center"><strong>TURMAS</strong></li>
	  <li class="list-group-item"><a href="<?php echo BASE.'home/turma/6' ?>">6° ano</a></li>
	  <li class="list-group-item"><a href="<?php echo BASE.'home/turma/7' ?>">7° ano</a></li>
	  <li class="list-group-item"><a href="<?php echo BASE.'home/turma/8' ?>">8° ano</a></li>
	  <li class="list-group-item"><a href="<?php echo BASE.'home/turma/9' ?>">9° ano</a></li>
	  <li class="list-group-item list-group-item-info text-center"><a href="<?php echo BASE.'home/professores/' ?>"><strong>PROFESSORES</strong></a></li>
	 </ul>
</div>

<div class="col-sm-9 col-md-9">
	<form method="POST" role="form">
			<legend>Adicionar Professor</legend>
		
			<div class="form-group">
				<label for="nome">Nome: </label>
				<input type="text" class="form-control" name="nome">
			</div>
			<br>
			<div class="form-group">
				<label for="nome">Matéria: </label>
				<input type="text" class="form-control" name="materia">
			</div>
			<br>
			<div class="form-group">
				<label for="nome">Email: </label>
				<input type="email" class="form-control" name="email">
			</div>
			<div class="form-group">
				<label for="nome">Senha: </label>
				<input type="password" class="form-control" name="senha">
			</div>	
			<br><br><br>
			<button type="submit" class="btn btn-info">Salvar</button>
		</form>	
</div>