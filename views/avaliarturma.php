
<h3>Bem vindo(a) professor(a) <a href="<?php echo BASE.'home';?>"><?php echo $professor['nome'] ?></a> <a class="btn btn-danger pull-right" href="<?php echo BASE.'home/logout' ?>"><span class="glyphicon glyphicon-off"></span> SAIR</a></h3><hr>

<div class="col-sm-3 col-md-3">
	<ul class="list-group">
	  <li class="list-group-item list-group-item-info text-center"><strong>TURMAS</strong></li>
	  <li class="list-group-item"><a href="<?php echo BASE.'home/turma/6' ?>">6° ano</a></li>
	  <li class="list-group-item"><a href="<?php echo BASE.'home/turma/7' ?>">7° ano</a></li>
	  <li class="list-group-item"><a href="<?php echo BASE.'home/turma/8' ?>">8° ano</a></li>
	  <li class="list-group-item"><a href="<?php echo BASE.'home/turma/9' ?>">9° ano</a></li>
	  <li class="list-group-item list-group-item-info text-center"><a href="<?php echo BASE.'home/professores/' ?>"><strong>PROFESSORES</strong></a></li>
	 </ul>
</div>

<div class="col-sm-9 col-md-9">
	<h3><?php echo $serie; ?>° ano</h3>

	<?php
		if (isset($aviso) && !empty($aviso)) {
			echo "<hr><h3>".$aviso."</h3><hr>";
		}
	?>
	
	<table class="table table-striped table-hover">
		<thead>
			<tr>
				<th>ID</th>
				<th>Nome</th>
				<th>Turma</th>
				<th>Ação</th>
			</tr>
		</thead>
		<tbody>
			<?php foreach ($turma as $alunos): ?>
			<tr>
				<td><?php echo $alunos['id']; ?></td>
				<td><?php echo $alunos['nome']; ?></td>
				<td><?php echo $alunos['turma']."° ano"; ?></td>
				<td>
					<a href="/descritivas/home/avaliar/<?php echo $alunos['id']; ?>/<?php echo $bimestre; ?>" class="btn btn-success"><span class="glyphicon glyphicon-user"></span> Avaliar</a>
					<a href="/descritivas/home/editar/<?php echo $alunos['id']; ?>" class="btn btn-warning"><span class="glyphicon glyphicon-edit"></span> Editar Avaliação</a>
				</td>
			</tr>
			<?php endforeach; ?>
		</tbody>
	</table>
	<br>
	<hr>
	
</div>