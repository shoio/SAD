
<h3>Bem vindo(a) professor(a) <a href="<?php echo BASE.'home';?>"><?php echo $professor['nome'] ?></a> <a class="btn btn-danger pull-right" href="<?php echo BASE.'home/logout' ?>"><span class="glyphicon glyphicon-off"></span> SAIR</a></h3><hr>

<div class="col-xs-6 col-sm-3 col-md-3">
	<ul class="list-group">
	  <li class="list-group-item list-group-item-info text-center"><strong>TURMAS</strong></li>
	  <li class="list-group-item"><a href="<?php echo BASE.'home/turma/6' ?>">6° ano</a></li>
	  <li class="list-group-item"><a href="<?php echo BASE.'home/turma/7' ?>">7° ano</a></li>
	  <li class="list-group-item"><a href="<?php echo BASE.'home/turma/8' ?>">8° ano</a></li>
	  <li class="list-group-item"><a href="<?php echo BASE.'home/turma/9' ?>">9° ano</a></li>
	  <li class="list-group-item list-group-item-info text-center"><a href="<?php echo BASE.'home/professores/' ?>"><strong>PROFESSORES</strong></a></li>
	 </ul>
</div>

<div class="col-xs-6 col-sm-9 col-md-9">
	<form action="" method="POST" role="form">
		<legend>Selecione o ano que deseja consultar</legend>
		<h6>*Caso um ano não seja selecionado, o ano corrente será setado automaticamente.</h6>
		<div class="form-group">
			<br>
			<label for="">Ano: </label>
			<select name="ano">
				<?php foreach ($anos as $ano): ?>
					<option value="<?php echo $ano; ?>" <?php if($ano == date('Y')){echo "selected";} ?>><?php echo $ano; ?></option>
				<?php endforeach; ?>
			</select>
		</div>
		
		<br>
		<button type="submit" class="btn btn-primary">Consultar</button>
	</form>
	<?php if (isset($aviso) && !empty($aviso)) {
		echo "<h4 style='color:green'>*".$aviso."</h4>";
	} ;?>
</div>

<h4 style="color: green"></h4>