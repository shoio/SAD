<h3>Bem vindo(a) professor(a) <a href="<?php echo BASE.'home';?>"><?php echo $professor['nome'] ?></a> <a class="btn btn-danger pull-right" href="<?php echo BASE.'home/logout' ?>"><span class="glyphicon glyphicon-off"></span> SAIR</a></h3><hr>

<div class="col-sm-3 col-md-3">
	<ul class="list-group">
	  <li class="list-group-item list-group-item-info text-center"><strong>TURMAS</strong></li>
	  <li class="list-group-item"><a href="<?php echo BASE.'home/turma/6' ?>">6° ano</a></li>
	  <li class="list-group-item"><a href="<?php echo BASE.'home/turma/7' ?>">7° ano</a></li>
	  <li class="list-group-item"><a href="<?php echo BASE.'home/turma/8' ?>">8° ano</a></li>
	  <li class="list-group-item"><a href="<?php echo BASE.'home/turma/9' ?>">9° ano</a></li>
	  <li class="list-group-item list-group-item-info text-center"><a href="<?php echo BASE.'home/professores/' ?>"><strong>PROFESSORES</strong></a></li>
	 </ul>
</div>

<div class="col-sm-9 col-md-9">
	<h3><?php echo $serie; ?>° ano</h3>
	<form action="" method="POST" role="form">
		<legend>Selecione o Bimestre</legend>
	
		<select name="bimestre" class="form-control" required="required">
			<option value="1">1° Bimestre</option>
			<option value="2">2° Bimestre</option>
			<option value="3">3° Bimestre</option>
			<option value="4">4° Bimestre</option>
		</select>
		<br>
		<button type="submit" class="btn btn-default">Salvar</button>
	</form>
	
</div>