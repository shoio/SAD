<?php

require 'environment.php';

define("BASE", 'http://localhost/descritivas/');

global $config;
$config = array();
if(ENVIRONMENT == "development"){
	$config['dbname'] = 'descritivas';
	$config['charset'] 	= 'utf8';
	$config['host'] = 'localhost';
	$config['dbuser'] = 'root';
	$config['dbpass'] = '';
}else{
	$config['dbname'] = '';
	$config['host'] = 'mysql.hostinger.com.br';
	$config['charset'] 	= 'utf8';
	$config['dbuser'] = '';
	$config['dbpass'] = '';
}


?>