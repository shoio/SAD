<?php
class Professores extends Model{
	
	public function login($email, $senha){
		$array = array();
		$sql = "SELECT * FROM professores WHERE email = '$email' AND senha = '$senha'";
		$sql = $this->db->query($sql);

		if ($sql->rowCount() > 0) {
			$array = $sql->Fetch();
			$_SESSION['lg'] = $array['id'];
		}
		return $array;
	}

	public function getDados(){
		$id = $_SESSION['lg'];
		
		$sql = "SELECT * FROM professores WHERE id = '$id'";
		$sql = $this->db->query($sql);

		if ($sql->rowCount() > 0) {
			
			return $sql->fetch();
		}
	}

	/*
	public function getMateria($id){
		$sql = "SELECT materia FROM professores WHERE id = '$id'";
		$sql = $this->db->query($sql);

		if ($sql->rowCount() > 0) {
			return $sql->fetch();
		}
	}
	*/

	public function getProfessores(){
		$sql = "SELECT * FROM professores";
		$sql = $this->db->query($sql);

		if ($sql->rowCount() > 0) {
			return $sql->fetchAll();
		}else{
			header("Location: /descritivas/inserirprofessor");
		}
	}

	public function getProfessor($id){
		$sql = "SELECT * FROM professores WHERE id = '$id'";
		$sql = $this->db->query($sql);

		if ($sql->rowCount() > 0) {
			
			return $sql->fetch();
		}
	}

	public function adicionarProfessor($nome, $materia, $email, $senha){
		$sql = "INSERT INTO professores SET nome = '$nome', materia = '$materia', email = '$email', senha = '$senha'";
		$sql = $this->db->query($sql);
	}

	public function deleteProfessor($id){
		$sql = "DELETE FROM professores WHERE id = '$id'";
		$sql = $this->db->query($sql);

		return true;
	}

	public function editarProfessor($id, $nome, $materia, $email, $senha){
		$sql = "UPDATE professores SET nome = '$nome', materia = '$materia', email = '$email', senha = '$senha' WHERE id = '$id'";
		$sql = $this->db->query($sql);

		return true;
	}
}