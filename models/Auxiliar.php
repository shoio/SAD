<?php
class Auxiliar extends Model{

	public function ifAuxiliar(){
		$sql = "SELECT * FROM auxiliar";
		$sql = $this->db->query($sql);

		if ($sql->rowCount() > 0) {
			return true;
		}else{
			return false;
		}
	}

	public function countAuxiliar(){
		$sql = "SELECT COUNT(*) FROM auxiliar";
		$sql = $this->db->query($sql);

		if ($sql->rowCount() > 0) {
			return $sql->fetch();
		}else{
			return 0;
		}
	}

	public function getAuxiliar(){
		$sql = "SELECT * FROM auxiliar WHERE status = '1' ORDER BY nome LIMIT 1";
		$sql = $this->db->query($sql);

		if ($sql->rowCount() > 0) {
			return $sql->fetch();
		}else{
			return "";
		}
	}

	public function inserirAuxiliar($id_aluno, $nome, $turma, $ano, $status){
		$sql = "INSERT INTO auxiliar SET id_aluno = '$id_aluno', nome = '$nome', turma = '$turma', ano = '$ano', status = '$status'";
		$sql = $this->db->query($sql);
	}

	public function deleteAuxiliar(){
		$sql = "DELETE FROM auxiliar WHERE status ='0'";
		$sql = $this->db->query($sql);
	}

	public function statusAuxiliar($id_aluno, $nome){
		$sql = "UPDATE auxiliar SET status = '0' WHERE id_aluno = '$id_aluno' AND nome = '$nome'";
		$sql = $this->db->query($sql);
	}

	public function zeroStatus(){
		$sql = "SELECT * FROM auxiliar WHERE status = '1'";
		$sql = $this->db->query($sql);

		if ($sql->rowCount() > 0) {
			return false;
		}else{
			return true;
		}
	}



}

	