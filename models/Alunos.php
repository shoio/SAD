<?php
class Alunos extends Model{

	private $aux;

	public function getAnos($anoCorrente){
		$sql = "SELECT ano FROM alunos WHERE ano != '$anoCorrente' GROUP BY ano ORDER BY ano DESC";
		$sql = $this->db->query($sql);

		if ($sql->rowCount() > 0) {
			return $sql->fetch();
		}
	}

	public function getTurma($turma, $ano){
		$array = array();
		$sql = "SELECT * FROM alunos WHERE turma = '$turma' AND ano = '$ano' ORDER BY nome";
		$sql = $this->db->query($sql);

		if ($sql->rowCount() > 0) {
			$array = $sql->fetchAll();
		}

		if (count($array) > 0) {
			return $array;
		}else{
			return false;
		}
	}

	public function getQtAlunos($turma, $ano){
		$sql = "SELECT COUNT(*) FROM alunos WHERE turma = '$turma' AND ano = '$ano'";
		$sql = $this->db->query($sql);

		if ($sql->rowCount() > 0) {
			return $sql->fetch();
		}
	}

	public function verificaTurma($id){
		$array = array();
		$sql = "SELECT turma FROM alunos WHERE id = '$id'";
		$sql = $this->db->query($sql);

		if ($sql->rowCount() > 0) {
			$array = $sql->fetch();
		}
		return $array[0];
	}

	public function deleteAluno($id){
		$sql = "DELETE FROM alunos WHERE id = '$id'";
		$sql = $this->db->query($sql);
	}

	public function getAluno($id){
		$sql = "SELECT * FROM alunos WHERE id = '$id'";
		$sql = $this->db->query($sql);

		if ($sql->rowCount() > 0) {
			return $sql->fetch();
		}
	}

	public function update($id, $nome, $turma){
		$sql = "UPDATE alunos SET nome = '$nome', turma = '$turma' WHERE id = '$id'";
		$sql = $this->db->query($sql);
	}

	public function adicionar($nome, $turma, $ano){
		$sql = "INSERT INTO alunos SET nome = '$nome', turma = '$turma', ano = '$ano'";
		$sql = $this->db->query($sql);
	}

	
}