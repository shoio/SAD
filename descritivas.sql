-- phpMyAdmin SQL Dump
-- version 4.7.2
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Tempo de geração: 21/02/2018 às 12:21
-- Versão do servidor: 10.1.26-MariaDB
-- Versão do PHP: 7.1.8

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Banco de dados: `descritivas`
--

-- --------------------------------------------------------

--
-- Estrutura para tabela `alunos`
--

CREATE TABLE `alunos` (
  `id` int(11) NOT NULL,
  `nome` varchar(150) NOT NULL,
  `turma` tinyint(4) NOT NULL,
  `ano` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Fazendo dump de dados para tabela `alunos`
--

INSERT INTO `alunos` (`id`, `nome`, `turma`, `ano`) VALUES
(1, 'Lorenzo Schoenardie', 9, 2018),
(12, 'Jubiraca', 7, 2018),
(13, 'Sholhos', 6, 2018),
(14, 'Papa Frita', 6, 2018),
(15, 'Coca Zero', 6, 2018),
(16, 'Marisa Véia Loka', 6, 2018),
(19, 'Clio 2001 Que A Jubiraca Deseja', 7, 2018),
(24, 'Cabeça De Papel', 7, 2018),
(25, 'Gonzalo Carneiro', 8, 2018),
(26, 'Joaõzinho', 8, 2018),
(27, 'Juliana Spindola', 6, 2018),
(29, 'Kanemann', 8, 2017),
(30, 'Geromel', 8, 2017),
(31, 'Maicon Desumano', 8, 2017),
(32, 'Edílson', 8, 2018),
(33, 'Ronivon', 6, 2018);

-- --------------------------------------------------------

--
-- Estrutura para tabela `auxiliar`
--

CREATE TABLE `auxiliar` (
  `id` int(11) NOT NULL,
  `id_aluno` int(11) NOT NULL,
  `nome` varchar(200) NOT NULL,
  `turma` tinyint(4) NOT NULL,
  `ano` int(11) NOT NULL,
  `status` tinyint(4) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estrutura para tabela `avaliacao`
--

CREATE TABLE `avaliacao` (
  `id` int(11) NOT NULL,
  `id_aluno` int(11) NOT NULL,
  `nome_aluno` varchar(200) NOT NULL,
  `serie` tinyint(11) NOT NULL,
  `bimestre` tinyint(4) NOT NULL,
  `ano` int(11) NOT NULL,
  `id_professor` int(11) NOT NULL,
  `nome_professor` varchar(200) NOT NULL,
  `perg_um` text NOT NULL,
  `resp_um` text NOT NULL,
  `perg_dois` text NOT NULL,
  `resp_dois` text NOT NULL,
  `perg_tres` text NOT NULL,
  `resp_tres` text NOT NULL,
  `perg_quatro` text NOT NULL,
  `resp_quatro` text NOT NULL,
  `perg_cinco` text NOT NULL,
  `resp_cinco` text NOT NULL,
  `perg_seis` text NOT NULL,
  `resp_seis` text NOT NULL,
  `perg_sete` text NOT NULL,
  `resp_sete` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Fazendo dump de dados para tabela `avaliacao`
--

INSERT INTO `avaliacao` (`id`, `id_aluno`, `nome_aluno`, `serie`, `bimestre`, `ano`, `id_professor`, `nome_professor`, `perg_um`, `resp_um`, `perg_dois`, `resp_dois`, `perg_tres`, `resp_tres`, `perg_quatro`, `resp_quatro`, `perg_cinco`, `resp_cinco`, `perg_seis`, `resp_seis`, `perg_sete`, `resp_sete`) VALUES
(213, 24, 'Cabeça De Papel', 7, 1, 2018, 1, 'Júlio', '1 - Demonstra interesse/atenção durante as aulas?', 'Quase sempre', '2 - Questiona e tira suas dúvidas?', 'Frequentemente não', '3 - Demonstra estar preparado nos momentos de atividades avaliativas?', 'Quase sempre', '4 - É pontual na entrega de trabalhos?', 'Frequentemente não', '5 - Faz os deveres de casa?', 'Quase sempre', '6 - Relaciona-se bem com os colegas?', 'Frequentemente não', '7 - Observações:', 'uhuhu');

-- --------------------------------------------------------

--
-- Estrutura para tabela `perguntas`
--

CREATE TABLE `perguntas` (
  `id` int(11) NOT NULL,
  `nick_pergunta` varchar(10) NOT NULL,
  `pergunta` varchar(200) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Fazendo dump de dados para tabela `perguntas`
--

INSERT INTO `perguntas` (`id`, `nick_pergunta`, `pergunta`) VALUES
(1, 'um', '1 - Demonstra interesse/atenção durante as aulas?'),
(2, 'dois', '2 - Questiona e tira suas dúvidas?'),
(3, 'tres', '3 - Demonstra estar preparado nos momentos de atividades avaliativas?'),
(4, 'quatro', '4 - É pontual na entrega de trabalhos?'),
(5, 'cinco', '5 - Faz os deveres de casa?'),
(6, 'seis', '6 - Relaciona-se bem com os colegas?'),
(7, 'sete', '7 - Observações:');

-- --------------------------------------------------------

--
-- Estrutura para tabela `professores`
--

CREATE TABLE `professores` (
  `id` int(11) NOT NULL,
  `nome` varchar(150) NOT NULL,
  `materia` varchar(60) NOT NULL,
  `email` varchar(150) NOT NULL,
  `senha` varchar(32) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Fazendo dump de dados para tabela `professores`
--

INSERT INTO `professores` (`id`, `nome`, `materia`, `email`, `senha`) VALUES
(1, 'Júlio', 'Inglês', 'bigshoio85@gmail.com', '74040ebee7cc95d535e13a9f21a3966f'),
(6, 'Rosânia', 'Matemática', 'rosania@gmail.com', 'f13c58cd3a587d7995463375a528dd4b');

-- --------------------------------------------------------

--
-- Estrutura para tabela `turmas`
--

CREATE TABLE `turmas` (
  `id` int(11) NOT NULL,
  `turma` int(11) NOT NULL,
  `ano` int(11) NOT NULL,
  `id_aluno` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Índices de tabelas apagadas
--

--
-- Índices de tabela `alunos`
--
ALTER TABLE `alunos`
  ADD PRIMARY KEY (`id`);

--
-- Índices de tabela `auxiliar`
--
ALTER TABLE `auxiliar`
  ADD PRIMARY KEY (`id`);

--
-- Índices de tabela `avaliacao`
--
ALTER TABLE `avaliacao`
  ADD PRIMARY KEY (`id`);

--
-- Índices de tabela `perguntas`
--
ALTER TABLE `perguntas`
  ADD PRIMARY KEY (`id`);

--
-- Índices de tabela `professores`
--
ALTER TABLE `professores`
  ADD PRIMARY KEY (`id`);

--
-- Índices de tabela `turmas`
--
ALTER TABLE `turmas`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT de tabelas apagadas
--

--
-- AUTO_INCREMENT de tabela `alunos`
--
ALTER TABLE `alunos`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=34;
--
-- AUTO_INCREMENT de tabela `auxiliar`
--
ALTER TABLE `auxiliar`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de tabela `avaliacao`
--
ALTER TABLE `avaliacao`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=214;
--
-- AUTO_INCREMENT de tabela `perguntas`
--
ALTER TABLE `perguntas`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT de tabela `professores`
--
ALTER TABLE `professores`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT de tabela `turmas`
--
ALTER TABLE `turmas`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
