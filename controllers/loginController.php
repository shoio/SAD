<?php
class loginController extends controller{

	public function __construct(){

	}
	
	public function index(){
		
		$dados = array('aviso' => '');

		if (isset($_POST['email']) && !empty($_POST['email'])) {
			$email = addslashes($_POST['email']);
			$senha = addslashes(md5($_POST['senha']));
			
			$p = new Professores();
		
			if ($p->login($email, $senha)) {
				header("Location: /descritivas");
			}else{
				$dados['aviso'] = "Seu email e/ou senha estão incorretos";
			}
		}

		$this->loadView('login', $dados);
	}
}