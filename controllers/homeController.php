<?php
class homeController extends controller{

	public function __construct(){
		
		if(!isset($_SESSION['lg']) || empty($_SESSION['lg'])){
			header("Location: /descritivas/login");
			exit;
		}
	}
	
	public function index(){
		
		$dados = array();

		$p = new Professores();
		$dados['professor'] = $p->getDados();

		$a = new Alunos();
		$anos = $a->getAnos(date('Y'));

		//Inserindo o ano corrente e o ano seguinte no início do array
		array_unshift($anos, date('Y')+1, date('Y'));

		//Excluindo valores repitidos do array
		$dados['anos'] =  array_unique($anos);

		if (isset($_POST['ano']) && !empty($_POST['ano'])) {
			$_SESSION['ano'] = addslashes($_POST['ano']);
		}else{
			$_SESSION['ano'] = date('Y');
		}

		$this->loadTemplate('home', $dados);
	}

	public function turma($turma){
		$dados = array();

		$p = new Professores();
		$dados['professor'] = $p->getDados();

		$t = new Alunos();

		if ($t->getTurma($turma, $_SESSION['ano'])) {
			$dados['turma'] = $t->getTurma($turma, $_SESSION['ano']);
			$dados['serie'] = $turma;

			$this->loadTemplate('turma', $dados);
		}else{
			header("Location: /descritivas/home/adicionar/$turma");
		}
	}

	public function delete($id){
		$t = new Alunos();
		$turma = $t->verificaTurma($id);
		$t->deleteAluno($id);

		header("location: /descritivas/home/turma/$turma");
	}

	public function editar($id){
		$dados = array();

		$p = new Professores();
		$dados['professor'] = $p->getDados();

		$a = new Alunos();
		$dados['aluno'] = $a->getAluno($id);

		if (isset($_POST['nome']) && !empty($_POST['nome'])) {
			$nome = addslashes(ucwords(strtolower($_POST['nome'])));
			$turma = addslashes($_POST['turma']);

			$a->update($id, $nome, $turma);
			header("Location: /descritivas/home/turma/$turma");
			exit;
		}

		$this->loadTemplate('editar', $dados);
	}

	public function adicionar($turma){
		$dados = array();

		$p = new Professores();
		$dados['professor'] = $p->getDados();
		$dados['turma'] = $turma;

		$a = new Alunos();

		if (isset($_POST['nome']) && !empty($_POST['nome'])) {
			$nome = addslashes(ucwords(strtolower($_POST['nome'])));
			$turma = addslashes($_POST['turma']);
			$ano = date('Y');

			$a->adicionar($nome, $turma, $ano);

			header("Location: /descritivas/home/turma/$turma");
			exit;
		}

		$this->loadTemplate('adicionar', $dados);
	}

	public function professores(){
		$dados = array();

		$p = new Professores();
		$dados['professor'] = $p->getDados();
		$dados['professores'] = $p->getProfessores();

		$this->loadTemplate('professores', $dados);
	}

	public function adicionarProfessor(){
		$dados = array();

		$p = new Professores();
		$dados['professor'] = $p->getDados();

		if (isset($_POST['nome']) && !empty($_POST['nome'])) {
			$nome = addslashes(ucwords(strtolower($_POST['nome'])));
			$materia = addslashes(ucwords(strtolower($_POST['materia'])));
			$email = addslashes($_POST['email']);
			$senha = addslashes(md5($_POST['senha']));
			
			$p->adicionarProfessor($nome, $materia, $email, $senha);

			header("Location: /descritivas/home/professores");
			exit;
		}

		$this->loadTemplate('adicionarprofessor', $dados);
	}

	public function deleteProfessor($id){
		$p = new Professores();

		if ($p->deleteProfessor($id)) {
			header("Location: /descritivas/home/professores");
		}
	}

	public function editarProfessor($id){
		$dados = array();

		$p = new Professores();
		$dados['professor'] = $p->getDados();
		$dados['profeditado'] = $p->getProfessor($id);

		if (isset($_POST['nome']) && !empty($_POST['nome'])) {
			$nome = addslashes(ucwords(strtolower($_POST['nome'])));
			$materia = addslashes(ucwords(strtolower($_POST['materia'])));
			$email = addslashes($_POST['email']);
			$senha = addslashes(md5($_POST['senha']));
			
			$p->editarProfessor($id, $nome, $materia, $email, $senha);

			header("Location: /descritivas/home/professores");
			exit;
		}

		$this->loadTemplate('editarprofessor', $dados);
	}

	public function selecionarbimestre($turma){
		$dados = array();

		$p = new Professores();
		$dados['professor'] = $p->getDados();
		$dados['serie'] = $turma;

		if (isset($_POST['bimestre']) && !empty($_POST['bimestre'])) {
			$bimestre = addslashes($_POST['bimestre']);
			$ano = $_SESSION['ano'];

			header("Location: /descritivas/home/avaliarturma/$turma/$bimestre/$ano");
		}

		$this->loadTemplate('selecionar', $dados);
	}

	public function avaliarturma($turma, $bimestre, $ano, $nome = ""){
		$dados = array();

		$p = new Professores();
		$dados['professor'] = $p->getDados();

		$t = new Alunos();

		if ($t->getTurma($turma, $ano)) {
			$dados['turma'] = $t->getTurma($turma, $ano);
			$dados['serie'] = $turma;
			$dados['bimestre'] = $bimestre;
			
			if (isset($nome) && !empty($nome)) {
				$nome = "O aluno(a)".$nome." foi avaliado com sucesso!";
				$dados['aviso'] = $nome;
			}
			
			$this->loadTemplate('avaliarturma', $dados);
		}else{
			header("Location: /descritivas/home/adicionar/$turma");
		}
	}

	public function avaliar($id, $bimestre){
		$dados = array();

		$p = new Professores();
		$dados['professor'] = $p->getDados();

		$pergunta = new Perguntas();
		$dados['perguntas'] = $pergunta->getPerguntas();

		$dados['bimestre'] = $bimestre;

		$aluno = new Alunos();
		$dados['aluno'] = $aluno->getAluno($id);
		$dados['serie'] = $aluno->verificaTurma($id);

		
		$this->loadTemplate('avaliar', $dados);

		
		if (isset($_POST['nome_p']) && !empty($_POST['nome_p'])) {
			$id_professor = addslashes($_POST['id_professor']);
			$nome_professor = addslashes($_POST['nome_p']);
			$materia = addslashes($_POST['materia']);
			$id_aluno = addslashes($_POST['id_aluno']);
			$nome_aluno = addslashes($_POST['nome_a']);
			$serie = $aluno->verificaTurma($id);
			$ano = date('Y');
							
			$perg_um = addslashes($_POST['perg_um']);
			$resp_um = addslashes($_POST['resp_um']);
			
			$perg_dois = addslashes($_POST['perg_dois']);
			$resp_dois = addslashes($_POST['resp_dois']);
				
			$perg_tres = addslashes($_POST['perg_tres']);
			$resp_tres = addslashes($_POST['resp_tres']);

			$perg_quatro = addslashes($_POST['perg_quatro']);
			$resp_quatro = addslashes($_POST['resp_quatro']);

			$perg_cinco = addslashes($_POST['perg_cinco']);
			$resp_cinco = addslashes($_POST['resp_cinco']);

			$perg_seis = addslashes($_POST['perg_seis']);
			$resp_seis = addslashes($_POST['resp_seis']);

			$perg_sete = addslashes($_POST['perg_sete']);
			$resp_sete = addslashes($_POST['resp_sete']);

			$avaliacao = new Avaliacoes();
			$avaliacao->inserir(	$id_aluno, 
									$nome_aluno, 
									$serie, 
									$bimestre, 
									$ano, 
									$id_professor, 
									$nome_professor, 
									$perg_um, 
									$resp_um, 
									$perg_dois, 
									$resp_dois, 
									$perg_tres, 
									$resp_tres, 
									$perg_quatro, 
									$resp_quatro, 
									$perg_cinco, 
									$resp_cinco, 
									$perg_seis, 
									$resp_seis, 
									$perg_sete, 
									$resp_sete		);

			/*
			$aviso = "O aluno(a) ".$nome_aluno." foi avaliado com sucesso!";
			
			header("Location: /descritivas/home/avaliarturma/$serie/$bimestre/$ano/$aviso");
			*/
		}	
	
	}

	public function logout(){
		unset($_SESSION['lg']);
		//unset($_SESSION['getturma']);
		header("Location: /descritivas/login");
	}

}